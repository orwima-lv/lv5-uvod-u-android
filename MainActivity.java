package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static java.util.Random.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etNewDescription;
    RadioButton rb1, rb2, rb3;
    ImageView ivIan, ivNikola, ivJohnny;
    Button btnInspiration;
    Button btnDescription;
    TextView tvIan, tvNikola, tvJohnny;

    public String IanInspiration = "I have no memory of this place.";
    public String JohnnyInspiration = "The code is more what you'd call 'guidelines' than actual rules.";
    public String NikolaInspiration = "I have not failed. I've just found 10,000 ways that won't work.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    protected void initializeUI(){
        this.ivIan = (ImageView) findViewById(R.id.ivIan);
        this.ivNikola = (ImageView) findViewById(R.id.ivNikola);
        this.ivJohnny = (ImageView) findViewById(R.id.ivJohnny);
        this.ivIan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivIan.setVisibility(View.INVISIBLE);
            }
        });
        this.ivNikola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivNikola.setVisibility(View.INVISIBLE);
            }
        });
        this.ivJohnny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivJohnny.setVisibility(View.INVISIBLE);
            }
        });

        this.btnInspiration = (Button) findViewById(R.id.btnInspiration);
        this.btnInspiration.setOnClickListener(this);


        this.tvIan = (TextView) findViewById(R.id.tvIanDesc);
        this.tvNikola = (TextView) findViewById(R.id.tvNikolaDesc);
        this.tvJohnny = (TextView) findViewById(R.id.tvJohnnyDesc);
        this.rb1 = (RadioButton) findViewById(R.id.rb1);
        this.rb2 = (RadioButton) findViewById(R.id.rb2);
        this.rb3 = (RadioButton) findViewById(R.id.rb3);
        this.etNewDescription = (EditText) findViewById(R.id.etNewDescription);
        this.btnDescription = (Button) findViewById(R.id.btnEdit);
        this.btnDescription.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ChangeDescription();
            }
        });
    }

    public void ChangeDescription(){
        String newDescription = etNewDescription.getText().toString();
        if(rb1.isChecked())
            this.tvIan.setText(newDescription);
        else if(rb2.isChecked())
            this.tvNikola.setText(newDescription);
        else
            this.tvJohnny.setText(newDescription);
    }

    @Override
    public void onClick(View v) {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(3);
        if(randomInt == 0)
            this.displayToast(IanInspiration);
        else if(randomInt == 1)
            this.displayToast(NikolaInspiration);
        else
            this.displayToast(JohnnyInspiration);
    }

    private void displayToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}